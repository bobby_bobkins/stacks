// Zak Fruth and Petr Shuller
// CS 240 with Ryan Parsons
// Stacks lab

import java.util.EmptyStackException;

// ListStack provides a complete stack implementation using a linked list.
// ListStack can only store doubles, and supports the operations push, pop, peek, and isEmpty
public class ListStack implements DStack{

    // Used to hold the index of the node on the top of the stack
    private LSNode front;
    
    // Constructor
    // Creates and returns an empty stack of doubles
    public ListStack(){
        front = null;
    }
    
    // Post: Returns true if the stack is empty
    public boolean isEmpty(){
        return front == null;
    }
    
    // Pre: Accepts a number of double precision
    // Post: Adds the number to the top of the stack
    public void push(double d){
        front = new LSNode(front, d);
    }
    
    // Pre: Throws an EmptyStackException if the stack is empty
    // Post: Removes and returns the number that is at the top of the stack
    public double pop(){
        if (isEmpty()){
            throw new EmptyStackException();
        }

        double temp = front.num;
        front = front.next;

        return temp;
    }
    
    // Pre: Throws an EmptyStackException if the stack is empty
    // Post: Returns the number that is at the top of ths stack
    public double peek(){
        if (isEmpty()){
            throw new EmptyStackException();
        }

        return front.num;
    }
    
    // Private inner node class used to create the linked list stack structure
    // Holds a single double as it's data as well as a pointer for the next node in the stack
    private class LSNode{
        private double num;
        private LSNode next;

        // Constructor
        // Pre: requires a node and a number
        private LSNode(LSNode q, double d){
            next = q;
            num = d;
        }
    }
}