// Zak Fruth and Petr Shuller
// CS 240 with Ryan Parsons
// Stacks lab

import java.util.EmptyStackException;

// ArrayStack provides a complete stack implementation using an array.
// ArrayStack can only store doubles, and supports the operations push, pop, peek, and isEmpty
public class ArrayStack implements DStack{

    // A default size for the array
    private static final int defaultLength = 128;

    // Store stack data in an array
    private double[] items;

    // Holds the current number of items on the stack
    private int size;

    // Default constructor
    // Creates and returns stack with default size
    public ArrayStack(){
        this(defaultLength);
    }

    // Constructor
    // Creates and returns stack with given size
    public ArrayStack(int length){
        items = new double[length];
        size = 0;
    }
   
    // Post: Returns true if the stack is empty
    public boolean isEmpty(){
      return size == 0;
   }

    // Pre: Accepts a number of double precision
    //      Increases the size of the array if it is full
    // Post: Adds the number to the top of the stack
    public void push(double d){
        if(size == items.length){
            doubleSize();
        }

        items[size] = d;
        size++;
     }
   
    // Pre: Throws an EmptyStackException if the stack is empty
    // Post: Removes and returns the number that is at the top of the stack
    public double pop(){
        if(isEmpty()){
            throw new EmptyStackException();
        }
        double temp = items[--size];
        items[size] = 0.0;

        return temp;
    }
   
    // Pre: Throws an EmptyStackException if the stack is empty
    // Post: Returns the number that is at the top of ths stack
    public double peek(){
        if(isEmpty()){
            throw new EmptyStackException();
        }

        return items[size-1];
    }
    
    // Post: Doubles the size of the array used for storing data
    private void doubleSize(){
        double[] temp = new double[items.length * 2];

        for(int i = 0; i < items.length; i++){
            temp[i] = items[i];
        }

        items = temp;
    }
}