import org.junit.Assert;
import org.junit.Test;
import java.util.EmptyStackException;



public class MyFirstTest {
    @Test
    public void firstTest() {
        Assert.assertTrue(true);
    }

    @Test
    public void testInitSize() {
    	ArrayStack as = new ArrayStack();
		ListStack ls = new ListStack();

		Assert.assertTrue(as.isEmpty());
		Assert.assertTrue(ls.isEmpty());
 	} 
 
 	@Test
    public void testPushPops() {
		// Gotta test both. Repetitive, but works.
    	ArrayStack as = new ArrayStack();
		ListStack ls = new ListStack();
	
		// Vals used for testing
		double v1 = 5;
		double v2 = 10;
	
		// Populate stack
		as.push(v1); ls.push(v1);
		as.push(v2); ls.push(v2);
		
		// Should no longer be empty
		Assert.assertFalse(as.isEmpty());
		Assert.assertFalse(ls.isEmpty());

		// Making sure vals at top are correct
		Assert.assertEquals(v2, as.peek(), 0);
		Assert.assertEquals(v2, ls.peek(),0);
    	
		// Should still be not empty
		Assert.assertFalse(as.isEmpty());
		Assert.assertFalse(ls.isEmpty());

		// Make sure we are popping the correct vals
		Assert.assertEquals(v2, as.pop(),0);
		Assert.assertEquals(v2, ls.pop(),0);
		
		// Should still not be empty
		Assert.assertFalse(as.isEmpty());
		Assert.assertFalse(ls.isEmpty());
	
		// Make sure we are looking at correct vals
		Assert.assertEquals(v1, as.peek(),0);
		Assert.assertEquals(v1, ls.peek(),0);

		// Should not be empty
		Assert.assertFalse(as.isEmpty());
		Assert.assertFalse(ls.isEmpty());

		// Making sure we popping correct vals
		Assert.assertEquals(v1, as.pop(),0);
		Assert.assertEquals(v1, ls.pop(),0);

		// Everything pushed now popped, should be empty
		Assert.assertTrue(as.isEmpty());
		Assert.assertTrue(ls.isEmpty());


		// Make sure proper exceptions thrown 
		// Need to do with try/catch from what bobby found
		try {
			as.peek();
			Assert.fail("Should not of been able to peak");
		} catch (EmptyStackException e){
		}

		try {
			ls.peek();
			Assert.fail("Should not of been able to peak");
		} catch (EmptyStackException e){
		}
		
		try {
			as.pop();
			Assert.fail("Should not of been able to pop");
		} catch (EmptyStackException e){
		}
	
		try {
			ls.pop();
			Assert.fail("Should not of been able to pop");
		} catch (EmptyStackException e){
		}
	}
}
