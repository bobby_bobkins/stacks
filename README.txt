Authors: Zak Fruth, Petr Shuller

How did you test that your stack implementations were correct?
	Did some unit testing. Used the JUnit unit testing framework. It was more of an integration test, but essentially made sure that the status of the stack at each point was as expected, and that proper exceptions were being thrown when required. Did stuff like make sure stack was initially empty, correct items were pushed, peeked, and popped, and made sure exceptions were being thrown when trying to peek or pop an empty stack.

Your array stacks start with a small array and double in size if they become full. For an input file with 1 million lines, where each line will be pushed on the stack, how many times would this resizing occur? What about with 1 billion lines or 1 trillion lines (assuming the computer had enough memory)? Explain your answer.
	Since our initial array size for our stack is 128, or 2^7, to hold 1 million elements, it would have to resize 13 times for a million elements, since 2^20 is the lowest power of 2 greater than 1 million and 20-7=13. A similar calculation for 1 billion and 1 trillion shows that they would take 23 and 33 resizings, respectively.

Suppose that, instead of a DStack interface, you were given a fully-functional FIFO Queue class. How might you implement this project (i.e., simulate a Stack) with one or more instances of a FIFO Queue?
	Since both queues and stacks maintain order based on the order that elements were added, the main storage of data, as well as push, operate by simply adding and storing them in a queue. The difference is in what is done when pop is is called. To access the most recent element added to the queue, we need to dequeue elements until the last element is reached and return that element, saving all other elements by enqueuing them into another queue and then setting that as our new storage queue.

Write pseudocode for your push and pop operations. Assume your Queue class provides the operations enqueue, dequeue, isEmpty, and size.
    push(element){
        enqueue the new element into our storage queue
    }

    pop(){
        if(storage queue isEmpty){
            throw exception
        }else{
            Queue tempQueue

            start at the front of the queue, go until the second to last element(using queue.size)){
                tempQueue.enqueue(storageQueue.dequeue)
            }

            // We can finally access top of stack
            Element e = storageQueue.dequeue
            storageQueue = tempQueue
            return e
        }
}


In the previous question, what trade-offs did you notice between a Queue implementation of a Stack and your original array-based implementation? Which implementation would you choose, and why?
	The trade-offs of using a queue implementation versus an array implementation are that the queue implementation is easier to create and there is possibly less wasted memory for storing the data while the array implementation has random access to any part of the stack, it is much easier to create a peek method without the need to hold onto another part of the queue, and pop has a constant runtime (as opposed to O(n) for the queue implementation) and doesn't need to create a second queue to operate. Overall, the array implementation is better, as having the O(1) runtime for both push and pop is ideal, as well as being able to peek without having to keep track of the back end of the queue.

What did you enjoy about this assignment? What did you not enjoy? What could you have done better?
	Felt fairly straightforward. Getting intellij setup was enjoyable, as was getting integration testing to work. Got to practice using git, so that was cool. An improvement for future would be to write tests before code. 

What else, if anything, would you would like to include related to this homework?
	A secret recipe, but it is secret. Oh and: 57 65 20 61 72 65 20 65 6e 6a 6f 79 69 6e 67 20 74 68 65 20 63 6c 61 73 73 21
